#!/bin/bash
# Author: umbokc

files=('fw')
options="-c"

param=" $options "

for((i=0;i<${#files[@]};i++))
do
	file=${files[$i]}
	param=" $param -w styl/$file.styl -o css/$file.css "
done

stylus $param
